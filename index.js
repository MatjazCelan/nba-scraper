if (process.argv[2]) {
  scrapeAndLog(process.argv[2]);
} else {
  console.log('Argument is missing');
}

async function scrapeAndLog(player) {
  const player3PA = await require('./scrape')(player);
  if (player3PA) log3PA(player3PA);
  else console.log('Something went wrong.');
  process.exit();
}

function log3PA(player3PA) {
  console.log("Player's 3PA:");
  player3PA.forEach((stats) => console.log(`${stats.year} ${stats['3PA']}`));
  console.log('');
}
