# NBA Web scraper

A simple web scraper command line tool to print a players 3PAs

To run the project:

First clone the repo, then cd into it, then run the following command:

Install dependencies using:

```
npm i
```

or

```
yarn
```

Then run:

```
npm run start "Player name"
```

or

```
yarn start "Player name"
```

The player name argument has to have the exact same characters as the player on the website. Letter casing doesn't matter.
