const puppeteer = require('puppeteer');

const searchBoxSelector =
  'input.border.border-bottlenose-dolphin.bg-white.p-2.t6.w-full.truncate.appearance-none.rounded';

module.exports = async (playerName) => {
  const browser = await puppeteer.launch();

  const page = await browser.newPage();
  await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64)');
  try {
    await page.goto('https://www.nba.com/players');
    await page.waitForSelector(searchBoxSelector);

    await page.type(searchBoxSelector, playerName);
    await page.waitForTimeout(500);

    const firstResult = await page.evaluate(getPlayerData);

    if (firstResult.name.toLowerCase() !== playerName.toLowerCase()) {
      console.log('That player does not exist!');
      return;
    }

    await page.goto(`https://www.nba.com/stats/player/${firstResult.playerId}`);
    await page.waitForTimeout(2000);

    return await page.evaluate(getPlayerSplitData);
  } catch (e) {
    throw e;
  }
};

async function getPlayerData() {
  const firstResult = document.querySelector('table.players-list a');
  const name = [];
  firstResult.querySelectorAll('p').forEach((p) => name.push(p.textContent));
  return {
    playerId: firstResult.getAttribute('href').split('/')[2],
    name: name.join(' '),
  };
}

async function getPlayerSplitData() {
  const tradSplitsTableBody = document.querySelector(
    '.nba-stat-table table tbody'
  );
  const data = [];
  tradSplitsTableBody?.querySelectorAll('tr').forEach((row) => {
    tableRow = row.querySelectorAll('td');
    data.push({
      year: tableRow[0].textContent.trim(),
      '3PA': tableRow[9].textContent.trim(),
    });
  });

  return data;
}
